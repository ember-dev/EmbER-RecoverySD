#!/bin/sh
#
######################################################################
##                                                                  ##
#                                                                    #
#   This script will create a Bootable Recovery SD card for EmbER.   #
#                                                                    #
#   Credits:                                                         #
#     - Guillermo A. Amaral B. <g@maral.me> rpi sd card builder      #
#     - OpenELEC's "create_sdcard" script                            #
#     - Stane1983 - http://www.stane1983.com                         #
#     - Christian Troy's guide on slatedroid.com                     #
#                                                                    #
#   Written by:	Josh.5                                               #
#   Date:	2015/11/15                                           #
#   Version:	2.0                                                  #
#                                                                    #
##                                                                  ##
######################################################################
#                                                                    #
#     usage:   sudo ./MakeRecoverySD.sh <drive>                      #
#     example: sudo ./MakeRecoverySD.sh /dev/sdb                     #
#                                                                    #
######################################################################

SDCARD="${1}"

usage() {
  clear
  echo "######################################################################"
  echo "##                                                                  ##"
  echo "#  Usage: ./MakeRecoverySD.sh <drive>                                #"
  echo "#    eg.  'sudo ./MakeRecoverySD.sh <drive>'                         #"
  echo '#    Where <drive> is your SD Card device node.                      #'
  echo '#    for example: /dev/sdb                                           #'
  echo '#    or /dev/mmcblk0c                                                #'
  echo '#                                                                    #'
  echo '#  NOTE:                                                             #'
  echo '#   You will require *root* privileges                               #'
  echo '#   in order to use this script.                                     #'
  echo '#                                                                    #'
  echo '#  NOTE2:                                                            #'
  echo '#   Ensure you run this file from the SAME folder as where it was    #'
  echo '#   installed, otherwise it will have problems running the scripts   #'
  echo '#                                                                    #'
  echo "##                                                                  ##"
  echo "######################################################################"
  echo
  df
  echo
  exit 0
}

confirm() {
  clear
  echo "####################################################################"
  echo "##                                                                ##"
  echo "#       By continuing with this script, you will totally           #"  
  echo "#       decimate the following device node: ${SDCARD}              #"
  echo '#                                                                  #'
  echo '#       If you are sure you want to continue?                      #'
  echo '#       (Please write "YES" in all caps)                           #'
  echo "##                                                                ##"
  echo "####################################################################"
  echo
  read -p 'ARE YOU SURE: ' CONTUNUE

  if [ "${CONTUNUE}" != "YES" ]; then
    echo
    echo "User didn't write \"YES\"... ABORTING!"
    exit 1
  fi
}

section() {
  echo "*****************************************************************************************"
  echo "> ${*}"
  echo "*****************************************************************************************"
  sleep 1
}

# environment overrides
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
OUTPUT_PREFIX=""

# check parameters
if [ -z "${SDCARD}" ] || [ "${SDCARD}" = "-h" ] || [ "${SDCARD}" = "--help" ] || [ ! -e MakeRecoverySD.sh ]; then
	usage
	exit 0
fi
if [ -z "${SDCARD}" ] || [ "${SDCARD}" = "-h" ] || [ "${SDCARD}" = "--help" ]; then
	usage
	exit 0
fi

# check if node is a block device
if [ ! -b "${SDCARD}" ]; then
	echo "${SDCARD} is not a block device!"
	exit 1
fi

# root privilege check
USERID=`id -u`
if [ ${USERID} -ne 0 ]; then
	echo "${0} requires root privileges in order to work."
	exit 0
fi

# dependencies
CP=`which cp`
FDISK=`which fdisk`
MKDIR=`which mkdir`
MKFS_VFAT=`which mkfs.vfat`
MOUNT=`which mount`
RM=`which rm`
RMDIR=`which rmdir`
SYNC=`which sync`
UNZIP=`which unzip`
UMOUNT=`which umount`
WGET=`which wget`

if [ -z "${CP}" ] ||
   [ -z "${WGET}" ] ||
   [ -z "${UNZIP}" ] ||
   [ -z "${FDISK}" ] ||
   [ -z "${MKDIR}" ] ||
   [ -z "${MKFS_VFAT}" ] ||
   [ -z "${MOUNT}" ] ||
   [ -z "${RMDIR}" ] ||
   [ -z "${RM}" ] ||
   [ -z "${UMOUNT}" ]; then
	echo "Missing dependencies:\n"
	echo "CP=${CP}"
	echo "FDISK=${FDISK}"
	echo "MKDIR=${MKDIR}"
	echo "MKFS_VFAT=${MKFS_VFAT}"
	echo "MOUNT=${MOUNT}"
	echo "RM=${RM}"
	echo "RMDIR=${RMDIR}"
	echo "UNZIP=${UNZIP}"
	echo "UMOUNT=${UMOUNT}"
	echo "WGET=${WGET}"
	exit 1
fi

# warn user
confirm

# prompt task
clear
echo "###########################################################"
echo "##                                                       ##"
echo "#    What device is this recvery SD being created for?    #"
echo "#                                                         #"
echo '#        1) Generic G02REF                                #'
echo '#        2) CMX                                           #'
echo '#        3) TLBBM6                                        #'
echo '#        4) G18REF                                        #'
echo '#                                                         #'
echo '#        x) Revert SD Card                                #'
echo "##                                                       ##"
echo "###########################################################"
echo
while true
do
    read -p 'Select device fro the list above:   ' AN
    case "$AN" in 
      1 ) echo; DEVICE="G02REF"; echo "Creating Recovery SD Card for ${DEVICE}..."; sleep 1; echo; break;;
      2 ) echo; DEVICE="CMX"; echo "Creating Recovery SD Card for ${DEVICE}..."; sleep 1; echo; break;;
      3 ) echo; DEVICE="TLBBM6"; echo "Creating Recovery SD Card for ${DEVICE}..."; sleep 1; echo; break;;
      4 ) echo; DEVICE="G18REF"; echo "Creating Recovery SD Card for ${DEVICE}..."; sleep 1; echo; break;;
      x ) echo; DEVICE="REVERT"; echo "Reverting SD Card..."; sleep 1; echo; break;;
      * ) echo "That was not a valid selection. Please try again. "; sleep 1; echo;;
esac
done


if [ "${DEVICE}" = "CMX" ]; then
  echo "###########################################################"
  echo "##                                                       ##"
  echo "#    Did you accidently install MX2/G18REF firmware       #"
  echo "#    and need to remove the bootloader from the nand?     #"
  echo "#                                                         #"
  echo "#    This normaly causes the box to easily corrupt        #"
  echo "#         or soft brick on random occasions.              #"
  echo "#                                                         #"
  echo '#                    yes / no                             #'
  echo "##                                                       ##"
  echo "###########################################################"

  while true
  do
    read -p 'Type "yes" or "no":   ' AN
    case "$AN" in 
      yes ) echo; CMXFIX="yes"; sleep 1; echo; break;;
      no ) echo; CMXFIX="no"; sleep 1; echo; break;;
      * ) echo "That was not a valid selection. Please try again. "; sleep 1; echo;;
  esac
  done
fi

clear
echo "##########################################################"
echo "##                                                      ##"
echo "#         EmbER Bootable Recovery SD Card Maker.         #"
echo "##                                                      ##"
echo "##########################################################"
echo "##                                                      ##"
echo "#     This will wipe any data off your chosen drive      #"
echo "#  Please be patient while we configure your SD Card...  #"
echo "#                                                        #"
echo "##########################################################"
echo
echo

case ${SDCARD} in
  "/dev/mmcblk"*)
    PART1="${SDCARD}p1"
    ;;
  *)
    PART1="${SDCARD}1"
    ;;
esac

# unmount the sd card
${UMOUNT} ${SDCARD}*

# wipe first part of sd card
dd if=/dev/zero of=${SDCARD} bs=512 count=2047




if [ "${DEVICE}" = "REVERT" ]; then
  section "Partitioning SD card..."
  ${FDISK} ${SDCARD} <<END
o
n
p
1


t
b
w
END
sleep 1
  # format partitions
  section "Formatting partitions..."
  ${MKFS_VFAT} -F32 "${PART1}" || exit 1



else
  # write bootloader
  section "Writing boot.img..."
  dd if=bootloaders/${DEVICE}_bootloader.img of="${SDCARD}" bs=640
  ${SYNC}
  sleep 2

  # partition sd card
  section "Partitioning SD card..."
  ${FDISK} ${SDCARD} <<END
u
d
n
p
1
3

t
b
a
w
END
  sleep 1
  # format partitions
  section "Formatting partitions..."
  ${MKFS_VFAT} -F32 "${PART1}" -I -n RECOVERY || exit 1

  # mount sd card
  section "Mounting partition ${PART1} ..."
  MOUNTPOINT=.mnt
  ${RM} -rf ${MOUNTPOINT}
  ${MKDIR} -p ${MOUNTPOINT}
  ${MOUNT} -t vfat "${PART1}" ${MOUNTPOINT}

  echo
  echo "##########################################################"
  echo "##                                                      ##"
  echo "#    Do you already have the latest conversion files     #"
  echo "#    for your device?                                    #"
  echo "#                                                        #"
  echo "#    This script can download them from EmbER-Dev.com    #"
  echo "#    and install them to your SD Card for you.           #"
  echo "##                                                      ##"
  echo "##########################################################"
  echo
  while true
    do
      read -p 'Would you like this script to download EmbER for your device and install in onto this SD Card? (y/n)   ' AN
      case "$AN" in 
        y|Y ) echo; DOWNLOAD="yes"; sleep 1; echo; break;;
        n|N ) echo; DOWNLOAD="no"; sleep 1; echo; break;;
        * ) echo "That was not a valid selection. Please try again. "; sleep 1; echo;;
    esac
  done

  if [ "${DOWNLOAD}" = "yes" ]; then
    ${MKDIR} -p ./firmware
    CONVERSION_ZIP=./firmware/${DEVICE}_conversion.zip

    if [ ! -e $CONVERSION_ZIP ]; then
      # download EmbER conversion files
      section "Downloading EmbER conversion files for $DEVICE..."
      LINK=$(curl "http://ember-dev.com/downloads/new_download.php?hardware=${DEVICE}" -A "Mozilla" -so - | grep -iPo "(id='script')(.*)(\>)" | grep -iPo "(http://)(.*)(\.zip)" | head -1)
      echo $LINK
      ${WGET} -O $CONVERSION_ZIP $LINK
    fi
    sleep 2

    # extract contents of EmbER conversion zip to SD Card
    section "Extracting files to $MOUNTPOINT..."
    ${UNZIP} $CONVERSION_ZIP -d $MOUNTPOINT
  fi

  if [ "${CMXFIX}" = "yes" ]; then
    rm -fv $MOUNTPOINT/aml_autoscript
    ${CP} -fv ./bootloaders/cmx_fix_aml_autoscript $MOUNTPOINT/aml_autoscript
  fi

  # sync disk
  section "Syncing disk..."
  ${SYNC}

  # unmount partition
  section "Unmounting partition $MOUNTPOINT ..."
  ${UMOUNT} ${PART1}

  # cleaning
  section "Cleaning tempdir..."
  ${RMDIR} $MOUNTPOINT

fi

section "Finished"
sleep 1

clear
echo "#########################################################"
echo "##                                                     ##"
echo "#         EmbER Bootable Recovery SD Card Maker         #"
echo "##                                                     ##"
echo "#########################################################"
echo "##                                                     ##"
echo "#        Your SD Card is now ready for use...           #"

if [ "${DOWNLOAD}" = "no" ]; then
  echo "#                                                       #"
  echo "#    Remove and then re-insert the card to continue.    #"
  echo "#                                                       #"
  echo "#  Dont forget to extract the EmbER Conversion zip to   #"
  echo "#   the root of this newly created Recovery SD Card.    #" 
fi

echo "#                                                       #"
echo "#########################################################"  
echo
echo
sleep 2

exit 0

